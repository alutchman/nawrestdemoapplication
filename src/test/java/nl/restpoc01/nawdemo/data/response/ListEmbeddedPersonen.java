package nl.restpoc01.nawdemo.data.response;

import lombok.Data;
import nl.restpoc01.nawdemo.data.messages.entity.response.PersonResponse;

import java.util.List;

@Data
public class ListEmbeddedPersonen {
    List<PersonResponse> personen;
}
