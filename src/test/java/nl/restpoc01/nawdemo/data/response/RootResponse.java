package nl.restpoc01.nawdemo.data.response;



import nl.restpoc01.nawdemo.data.messages.HrefForLinksTemplated;

import java.util.HashMap;
import java.util.Map;

public class RootResponse {
    private Map<String, HrefForLinksTemplated> _links = new HashMap<>();

    public Map<String, HrefForLinksTemplated> get_links() {
        return _links;
    }

    public void set_links(Map<String, HrefForLinksTemplated> _links) {
        this._links = _links;
    }
}
