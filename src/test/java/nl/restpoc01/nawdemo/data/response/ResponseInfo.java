package nl.restpoc01.nawdemo.data.response;

import org.springframework.http.ResponseEntity;

import java.io.IOException;

public class ResponseInfo {
    private final int code;
    private final boolean successful;
    private final String body;
    private final String message;




    public ResponseInfo(ResponseEntity<String> response) {
        successful= response.getStatusCode().is2xxSuccessful() ;
        code = response.getStatusCodeValue();
        body = response.getBody();
        message = null;
    }


    public int getCode() {
        return code;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public String getBody() {
        return body;
    }

    public String getMessage() {
        return message;
    }
}
