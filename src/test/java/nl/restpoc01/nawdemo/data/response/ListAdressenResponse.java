package nl.restpoc01.nawdemo.data.response;

import lombok.Data;
import nl.restpoc01.nawdemo.data.messages.HrefForLinksTemplated;

import java.util.HashMap;
import java.util.Map;


@Data
public class ListAdressenResponse {
    private ListEmbeddedAdressen _embedded;

    private Map<String, HrefForLinksTemplated> _links = new HashMap<>();




}
