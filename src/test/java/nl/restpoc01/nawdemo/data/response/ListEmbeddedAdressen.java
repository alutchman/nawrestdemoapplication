package nl.restpoc01.nawdemo.data.response;

import lombok.Data;
import nl.restpoc01.nawdemo.data.messages.entity.response.AdressResponse;

import java.util.List;

@Data
public class ListEmbeddedAdressen {
    List<AdressResponse> adressen;
}
