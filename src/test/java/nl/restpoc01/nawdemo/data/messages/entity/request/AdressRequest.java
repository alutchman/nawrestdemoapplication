package nl.restpoc01.nawdemo.data.messages.entity.request;

public class AdressRequest {
    private final String zipcode;
    private final int number;
    private final String extension;
    private final String city;
    private final String phone;
    private final String streetname;

    public AdressRequest(String zipcode, int number, String extension, String city, String streetname, String phone) {
        this.zipcode = zipcode;
        this.number = number;
        this.extension = extension;
        this.city = city;
        this.streetname = streetname;
        this.phone =phone;
    }

    public String getZipcode() {
        return zipcode;
    }

    public int getNumber() {
        return number;
    }

    public String getExtension() {
        return extension;
    }

    public String getCity() {
        return city;
    }

    public String getStreetname() {
        return streetname;
    }
}
