package nl.restpoc01.nawdemo.data.messages;

public class ExceptionCauseResponse {
    private ExceptionCauseResponse cause;
    private String message;

    public ExceptionCauseResponse getCause() {
        return cause;
    }

    public void setCause(ExceptionCauseResponse cause) {
        this.cause = cause;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
