package nl.restpoc01.nawdemo.data.messages.entity.response;

import lombok.Data;
import nl.restpoc01.nawdemo.data.messages.BaseResponse;

import java.util.Date;

@Data
public class PersonResponse extends BaseResponse {
    private String achternaam;
    private String voornaam;

    private Date geboortedatum;
}
