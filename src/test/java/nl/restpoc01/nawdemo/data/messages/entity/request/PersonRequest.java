package nl.restpoc01.nawdemo.data.messages.entity.request;

import lombok.Data;

import java.util.Date;

@Data
public class PersonRequest {

    private String achternaam;
    private String voornaam;
    private Date geboortedatum;

    private String adres;



}
