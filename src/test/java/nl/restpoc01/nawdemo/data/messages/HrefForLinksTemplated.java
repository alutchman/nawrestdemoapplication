package nl.restpoc01.nawdemo.data.messages;

public class HrefForLinksTemplated {
    private String href;
    private boolean templated;

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public boolean isTemplated() {
        return templated;
    }

    public void setTemplated(boolean templated) {
        this.templated = templated;
    }
}
