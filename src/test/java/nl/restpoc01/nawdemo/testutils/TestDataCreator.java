package nl.restpoc01.nawdemo.testutils;

import lombok.extern.slf4j.Slf4j;
import nl.restpoc01.nawdemo.data.messages.entity.request.AdressRequest;
import nl.restpoc01.nawdemo.data.messages.entity.request.PersonRequest;
import nl.restpoc01.nawdemo.data.messages.entity.response.AdressResponse;
import nl.restpoc01.nawdemo.data.response.ResponseInfo;
import nl.restpoc01.nawdemo.utils.JsonConvertUtility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
public class TestDataCreator {
    public static AdressResponse addAdress(String baseUri, String zipcode, Integer number, String extension, String city, String straatnaam) {
        AdressRequest data = new AdressRequest(zipcode, number, extension, city,
                straatnaam, null);
         ResponseInfo responseInfo = TestRequestHttpHandler.addAdress(baseUri+"/adres", data);

        if (!responseInfo.isSuccessful()) {
            log.info("HTTP code = {}", responseInfo.getCode());
            return null;
        }
        AdressResponse result = JsonConvertUtility.getJsonObject(responseInfo.getBody(), AdressResponse.class);
        return result;
    }

    public static void addPersoon(String baseUri, String voornaam, String achternaam, String geboren, Long adressID) {
        PersonRequest personRequest = new PersonRequest();
        personRequest.setVoornaam(voornaam);
        personRequest.setAchternaam(achternaam);
        if (adressID != null) {
            personRequest.setAdres(baseUri+ "/adres/"+adressID);
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dateInString = geboren;
        try {
            Date datum = sdf.parse(dateInString);
            personRequest.setGeboortedatum(datum);
        } catch (ParseException e) {

        }
        TestRequestHttpHandler.addPerson(baseUri+"/persoon", personRequest);
    }
}
