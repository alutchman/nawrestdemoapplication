package nl.restpoc01.nawdemo.testutils;

import lombok.extern.slf4j.Slf4j;
import nl.restpoc01.nawdemo.data.messages.entity.request.AdressRequest;
import nl.restpoc01.nawdemo.data.messages.entity.request.PersonRequest;
import nl.restpoc01.nawdemo.data.response.ResponseInfo;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@Slf4j
public class TestRequestHttpHandler {
    private static TestRestTemplate restTemplate = new TestRestTemplate();
    private static HttpHeaders headers = new HttpHeaders();
    static
    {
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");
    }


    public static ResponseInfo handleGet(String  uri){
        ResponseEntity<String> response = restTemplate
                .withBasicAuth("user", "password")
                .getForEntity(uri, String.class);
        ResponseInfo result = new ResponseInfo(response);
        return result;
    }

    public static ResponseInfo addAdress(String uri, AdressRequest adressRequest) {
        final HttpEntity<AdressRequest> requestEntity = new HttpEntity<>(adressRequest);

        ResponseEntity<String> response =
                restTemplate
                        .withBasicAuth("admin", "password")
                        .exchange(uri, HttpMethod.POST, requestEntity, String.class);
        ResponseInfo result = new ResponseInfo(response);

        return result;

    }

    public static ResponseInfo addPerson(String uri, PersonRequest personRequest) {
        final HttpEntity<PersonRequest> requestEntity = new HttpEntity<>(personRequest,headers);

        ResponseEntity<String> response = restTemplate
                .withBasicAuth("admin", "password")
                .postForEntity(uri,requestEntity, String.class);
        ResponseInfo result = new ResponseInfo(response);

        return result;
    }
}
