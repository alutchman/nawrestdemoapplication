package nl.restpoc01.nawdemo;

import lombok.extern.slf4j.Slf4j;
import nl.restpoc01.nawdemo.data.messages.ExceptionCauseResponse;
import nl.restpoc01.nawdemo.data.messages.entity.response.AdressResponse;
import nl.restpoc01.nawdemo.data.messages.entity.response.PersonResponse;
import nl.restpoc01.nawdemo.data.response.ListAdressenResponse;
import nl.restpoc01.nawdemo.data.response.ListPersonenResponse;
import nl.restpoc01.nawdemo.data.response.ResponseInfo;
import nl.restpoc01.nawdemo.data.response.RootResponse;
import nl.restpoc01.nawdemo.data.responses.ErrorResponse;
import nl.restpoc01.nawdemo.testutils.TestDataCreator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.ResourceAccessException;

import java.util.ArrayList;
import java.util.List;

import static nl.restpoc01.nawdemo.testutils.TestRequestHttpHandler.*;
import static nl.restpoc01.nawdemo.utils.JsonConvertUtility.getJsonObject;
import static org.junit.Assert.*;

@Slf4j
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NawdemoApplicationTests {
    private static final int PORTNUMBER = 65535;
    private static final String PORTARG = "--server.port="+PORTNUMBER;
    private static final String BASE_URL = "http://localhost:"+PORTNUMBER;

    private static AdressResponse result1;
    private static AdressResponse result2;
    private static AdressResponse result3;
    private static List<AdressResponse> resultList;

    private static TestRestTemplate restTemplate = new TestRestTemplate();
    private static HttpHeaders headers = new HttpHeaders();
    static
    {
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");

    }

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @BeforeClass
    public static void init(){
        String[] args = new String[]{PORTARG};
        NawdemoApplication.main(args);
    }

    @Before
    public void befoteTest(){
        if (resultList == null) {

            resultList = new ArrayList<>();
            resultList.add(TestDataCreator.addAdress(BASE_URL,"2806EB", 55, "", "Gouda",
                    "Poldermolenerf"));
            resultList.add(TestDataCreator.addAdress(BASE_URL,"2907DD", 11, "bis", "Capelle aan den IJssel",
                    "Grafiek"));
            resultList.add(TestDataCreator.addAdress(BASE_URL,"2525KH", 4, "", "Den Haag",
                    "Fruitweg"));
            resultList.add(TestDataCreator.addAdress(BASE_URL,"2525KH", 6, "", "Den Haag",
                    "Fruitweg"));

            result1 = resultList.get(0);
            result2 = resultList.get(1);
            result3 = resultList.get(2);

            TestDataCreator.addPersoon(BASE_URL,"Donald","Trompet","1960-12-01",2L);
            TestDataCreator.addPersoon(BASE_URL,"Katrien","Trompet","1970-11-01",2L);
            
        }
    }

    @Test
    public void test000_GetDefinedID0_AllEntities(){
        ResponseInfo responseInfo = handleGet(BASE_URL);
        RootResponse result = getJsonObject(responseInfo.getBody(), RootResponse.class);
        assertNotNull(result.get_links().get("adressen"));
        assertNotNull(result.get_links().get("personen"));
        log.info(responseInfo.getBody());
    }


    @Test
    public void test001_GetDefinedID1(){
        ResponseInfo responseInfo = handleGet(BASE_URL +"/adres/1");
        log.info(responseInfo.getBody());
        AdressResponse adressResponse = getJsonObject(responseInfo.getBody(), AdressResponse.class);
        assertNotNull(adressResponse);
    }


    @Test
    public void test001_GetDefinedID2(){
        ResponseInfo responseInfo = handleGet(BASE_URL +"/adres/2");

        AdressResponse adressResponse = getJsonObject(responseInfo.getBody(), AdressResponse.class);
        assertNotNull(adressResponse);
        assertNotNull(adressResponse.get_links().get("personen").getHref());

        String uriPersonen = adressResponse.get_links().get("personen").getHref();
        responseInfo = handleGet(uriPersonen);


        log.info(responseInfo.getBody());

        ListPersonenResponse resultList = getJsonObject(responseInfo.getBody(), ListPersonenResponse.class);
        assertNotNull(resultList);

        List<PersonResponse>  personen = resultList.get_embedded().getPersonen();
        assertEquals(2, personen.size());

        assertEquals("Trompet",personen.get(0).getAchternaam());
        assertEquals("Donald",personen.get(0).getVoornaam());

        assertEquals("Trompet",personen.get(1).getAchternaam());
        assertEquals("Katrien",personen.get(1).getVoornaam());
    }

    @Test
    public void test001_GetDefinedID3(){
        ResponseInfo responseInfo = handleGet(BASE_URL +"/persoon/1");
        PersonResponse person1 = getJsonObject(responseInfo.getBody(), PersonResponse.class);
        assertNotNull(person1);
    }


    @Test
    public void test001_FindByParams0(){
        ResponseInfo responseInfo = handleGet(BASE_URL +"/adres/search/findByZipcode?zipcode=3000AH");
        assertNotNull(responseInfo.getBody());

        ListAdressenResponse resultList = getJsonObject(responseInfo.getBody(), ListAdressenResponse.class);
        assertNotNull(resultList);
        assertNotNull(resultList.get_embedded().getAdressen());
        List<AdressResponse> lstAdresses = (List<AdressResponse>) resultList.get_embedded().getAdressen();
        assertEquals(0, lstAdresses.size());
    }



    @Test
    public void test001_FindByParams1(){
        ResponseInfo responseInfo = handleGet(BASE_URL +"/adres/search/findByZipcode?zipcode=2525KH");

        assertNotNull(responseInfo.getBody());

        ListAdressenResponse resultList = getJsonObject(responseInfo.getBody(), ListAdressenResponse.class);
        assertNotNull(resultList);
        assertNotNull(resultList.get_embedded().getAdressen());

        List<AdressResponse> lstAdresses = (List<AdressResponse>) resultList.get_embedded().getAdressen();
        assertEquals(2, lstAdresses.size());
    }

    @Test
    public void test001_FindByParams2(){
        ResponseInfo responseInfo = handleGet(BASE_URL +"/persoon/search/zoekopachternaam?achternaam=Trompet");
        assertNotNull(responseInfo.getBody());
        ListPersonenResponse resultList = getJsonObject(responseInfo.getBody(), ListPersonenResponse.class);
        assertNotNull(resultList);
        assertEquals(2, resultList.get_embedded().getPersonen().size());
    }

    @Test
    public void test002_DeleteValidId(){
        String urlSearch = BASE_URL + "/adres/search/findByZipcodeNumber?zipcode=2525KH&number=6";
        ResponseInfo responseInfo = handleGet(urlSearch);
        log.info(responseInfo.getBody());

        ListAdressenResponse resultList0 = getJsonObject(responseInfo.getBody(), ListAdressenResponse.class);
        assertNotNull(resultList0);
        assertNotNull(resultList0.get_embedded().getAdressen());
        List<AdressResponse> lstAdresses0 = (List<AdressResponse>) resultList0.get_embedded().getAdressen();
        assertEquals(1, lstAdresses0.size());

        String targetUrl = resultList0.get_embedded().getAdressen().get(0).get_links().get("self").getHref();;
        HttpEntity<String> request = new HttpEntity<String>(headers);
        ResponseEntity<String> result0 = null;
        result0 = restTemplate.withBasicAuth("admin", "password")
                .exchange(targetUrl, HttpMethod.DELETE, request, String.class);
        assertEquals(204, result0.getStatusCodeValue());
        assertNull(result0.getBody());

        responseInfo = handleGet(urlSearch);
        log.info(responseInfo.getBody());
        ListAdressenResponse resultList = getJsonObject(responseInfo.getBody(), ListAdressenResponse.class);
        assertNotNull(resultList);
        assertNotNull(resultList.get_embedded().getAdressen());
        List<AdressResponse> lstAdresses = (List<AdressResponse>) resultList.get_embedded().getAdressen();
        assertEquals(0, lstAdresses.size());
    }

    @Test
    public void test002_DeleteValidIdNoAuth(){
        String targetUrl = BASE_URL + "/adres/9999";
        HttpEntity<String> request = new HttpEntity<String>(headers);
        ResponseEntity<String> result0 = null;
        result0 = restTemplate.withBasicAuth("user", "password")
                .exchange(targetUrl, HttpMethod.DELETE, request, String.class);
        assertEquals(403, result0.getStatusCodeValue());
        assertNotNull(result0.getBody());

        ErrorResponse errorResponse = getJsonObject(result0.getBody(), ErrorResponse.class);

        assertNotNull(errorResponse);
        assertEquals(403, errorResponse.getStatus().intValue());
        assertEquals("Forbidden", errorResponse.getMessage());
    }


    @Test
    public void test002_DeleteInvalidId(){
        String targetUrl = BASE_URL+ "/adres/111";

        HttpEntity<String> request = new HttpEntity<String>(headers);
        ResponseEntity<String> result0 = restTemplate.withBasicAuth("admin", "password")
                .exchange(targetUrl, HttpMethod.DELETE, request, String.class);
        assertEquals(404, result0.getStatusCodeValue());
        assertNull(result0.getBody());
    }


    @Test
    public void test003_InvalidService(){
        String targetUrl = BASE_URL+ "/unknown/1";

        ResponseEntity<String> result0 = restTemplate.withBasicAuth("admin", "password").getForEntity(targetUrl, String.class);
        assertEquals(404, result0.getStatusCodeValue());
        String data = result0.getBody();
        System.out.println(data);
        ErrorResponse errorResponse = getJsonObject(data, ErrorResponse.class);

        assertNotNull(errorResponse);
        assertEquals(404, errorResponse.getStatus().intValue());
        assertEquals("No message available", errorResponse.getMessage());

    }

    @Test
    public void test006_findNoAdress() {
        String targetUrl = BASE_URL+ "/adres/4";
        ResponseEntity<String> result0 = restTemplate.withBasicAuth("user", "password")
                .getForEntity(targetUrl, String.class);
        assertEquals(404, result0.getStatusCodeValue());
        assertNull(result0.getBody());
    }

    @Test
    public void test007_post404Error() {
        final String uri = BASE_URL +"/postdummy.json";
        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<String> response = restTemplate.withBasicAuth("admin", "password").postForEntity(uri, entity, String.class);
        assertNotNull(response.getBody());
        String responstTxt = response.getBody();
        log.info(responstTxt);

        ErrorResponse result = getJsonObject(responstTxt, ErrorResponse.class);
        assertNotNull(result);
        assertEquals("No message available", result.getMessage());
        assertEquals(404, result.getStatus().intValue());
    }


    @Test
    public void test009_PatchAsdresObjectOK(){
        String uri =  BASE_URL +"/adres/1";
        ResponseInfo responseInfo = handleGet(uri);
        AdressResponse adressResponse = getJsonObject(responseInfo.getBody(), AdressResponse.class);
        assertNotNull(adressResponse);
        assertNull(adressResponse.getPhone());

        adressResponse.setPhone("0123456789");

        // Create HttpEntity
        final HttpEntity<AdressResponse> requestEntity = new HttpEntity<>(adressResponse);

        ResponseEntity<String> response =
                restTemplate.withBasicAuth("admin", "password")
                        .exchange(uri, HttpMethod.PATCH, requestEntity, String.class);

        assertEquals(200, response.getStatusCodeValue());

        responseInfo = handleGet(uri);

        AdressResponse adressResponse1 = getJsonObject(responseInfo.getBody(), AdressResponse.class);
        assertNotNull(adressResponse1);
        assertNotNull(adressResponse1.getPhone());
        assertEquals("0123456789", adressResponse1.getPhone());
    }

    @Test
    public void test010_PatchAsdresObjectOK(){
        String uri =  BASE_URL +"/adres/1";
        ResponseInfo responseInfo = handleGet(uri);
        AdressResponse adressResponse = getJsonObject(responseInfo.getBody(), AdressResponse.class);
        assertNotNull(adressResponse);

        adressResponse.setPhone("01040012345");

        // Create HttpEntity
        final HttpEntity<AdressResponse> requestEntity = new HttpEntity<>(adressResponse);

        ResponseEntity<String> response =
                restTemplate.withBasicAuth("admin", "password")
                        .exchange(uri, HttpMethod.PATCH, requestEntity, String.class);

        assertEquals(200, response.getStatusCodeValue());
        log.info(response.getBody());

        AdressResponse adressResponse1 = getJsonObject(response.getBody(), AdressResponse.class);
        assertNotNull(adressResponse1);
        assertNotNull(adressResponse1.getPhone());
        assertEquals("01040012345", adressResponse1.getPhone());
    }


    @Test
    public void test011_PatchAsdresObjectNotOK(){
        String uri =  BASE_URL +"/adres/1";
        ResponseInfo responseInfo = handleGet(uri);
        AdressResponse adressResponse = getJsonObject(responseInfo.getBody(), AdressResponse.class);
        assertNotNull(adressResponse);


        adressResponse.setZipcode("2907DD");
        adressResponse.setNumber(11);
        adressResponse.setExtension("bis");
        adressResponse.setPhone("0123456789");

        // Create HttpEntity
        final HttpEntity<AdressResponse> requestEntity = new HttpEntity<>(adressResponse);

        ResponseEntity<String> response =
                restTemplate.withBasicAuth("admin", "password")
                        .exchange(uri, HttpMethod.PATCH, requestEntity, String.class);

        assertEquals(409, response.getStatusCodeValue());
        log.info(response.getBody());

        ExceptionCauseResponse exceptionCauseResponse = getJsonObject(response.getBody(), ExceptionCauseResponse.class);
        assertNotNull(exceptionCauseResponse);
        assertTrue(exceptionCauseResponse.getMessage().startsWith("could not execute statement; SQL [n/a];"));
        assertNull(exceptionCauseResponse.getCause().getCause().getCause());
        assertTrue(exceptionCauseResponse.getCause().getCause().getMessage().startsWith("Unique index or primary key violation: "));
    }


    @Test
    public void test012_PatchAsdresObjectInvalidID(){
        String uri =  BASE_URL  +"/adres/1";
        ResponseInfo responseInfo = handleGet(uri);
        AdressResponse adressResponse = getJsonObject(responseInfo.getBody(), AdressResponse.class);
        assertNotNull(adressResponse);

        adressResponse.setZipcode("2907DD");
        adressResponse.setNumber(11);
        adressResponse.setExtension("bis");
        adressResponse.setPhone("0123456789");



        String InvaliudUri =  BASE_URL +"/adres/99991";

        // Create HttpEntity
        final HttpEntity<AdressResponse> requestEntity = new HttpEntity<>(adressResponse);

        ResponseEntity<String> response =
                restTemplate.withBasicAuth("admin", "password")
                        .exchange(InvaliudUri, HttpMethod.PATCH, requestEntity, String.class);

        assertEquals(404, response.getStatusCodeValue());
        assertNull(response.getBody());
    }

    @Test
    public void test101ForClosingContext() {
        final String uri = BASE_URL +"/shutdownContext";

       expectedException.expect(ResourceAccessException.class);
       expectedException.expectMessage(String.format("I/O error on POST request for \"%s\"", uri));

        HttpEntity<String> entity = new HttpEntity<>(headers);
        restTemplate.withBasicAuth("user", "password").postForEntity(uri, entity, String.class);
    }


    @Test
    public void test102_AfterClosingContext() {
        final String uri = BASE_URL +"/postdummy.json";

        expectedException.expect(ResourceAccessException.class);
        expectedException.expectMessage(String.format("I/O error on POST request for \"%s\"", uri));

        HttpEntity<String> entity = new HttpEntity<>(headers);
        restTemplate.withBasicAuth("user", "password").postForEntity(uri, entity, String.class);
    }

}
