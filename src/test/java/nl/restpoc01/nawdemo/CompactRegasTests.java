package nl.restpoc01.nawdemo;


import lombok.extern.slf4j.Slf4j;
import nl.restpoc01.nawdemo.data.messages.entity.response.AdressResponse;
import nl.restpoc01.nawdemo.data.messages.entity.response.PersonResponse;
import nl.restpoc01.nawdemo.data.response.ListAdressenResponse;
import nl.restpoc01.nawdemo.data.response.ListPersonenResponse;
import nl.restpoc01.nawdemo.data.response.ResponseInfo;
import nl.restpoc01.nawdemo.data.responses.ErrorResponse;
import nl.restpoc01.nawdemo.testutils.TestDataCreator;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static nl.restpoc01.nawdemo.testutils.TestRequestHttpHandler.handleGet;
import static nl.restpoc01.nawdemo.utils.JsonConvertUtility.getJsonObject;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Slf4j
@Ignore
public class CompactRegasTests {
    private static final int PORTNUMBER = 8080;
    private static final String BASE_URL = "http://localhost:"+PORTNUMBER;
    private static TestRestTemplate restTemplate = new TestRestTemplate();
    private static HttpHeaders headers = new HttpHeaders();
    static
    {
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");

    }

    @Test
    public void prepareData(){
        TestDataCreator.addAdress(BASE_URL,"2806EB", 55, "", "Gouda",
                "Poldermolenerf");
        TestDataCreator.addAdress(BASE_URL,"2907DD", 11, "bis", "Capelle aan den IJssel",
                "Grafiek");
        TestDataCreator.addAdress(BASE_URL,"2525KH", 4, "", "Den Haag",
                "Fruitweg");
        TestDataCreator.addAdress(BASE_URL,"2525KH", 6, "", "Den Haag",
                "Fruitweg");

        TestDataCreator.addPersoon(BASE_URL,"Donald","Trompet","1960-12-01",2L);
        TestDataCreator.addPersoon(BASE_URL,"Katrien","Trompet","1970-11-01",2L);

    }

    @Test
    public void test2525KH(){
        ResponseInfo responseInfo = handleGet(BASE_URL +"/adres/search/findByZipcode?zipcode=2525KH");
        assertNotNull(responseInfo.getBody());

        ListAdressenResponse resultList = getJsonObject(responseInfo.getBody(), ListAdressenResponse.class);
        assertNotNull(resultList);
        assertNotNull(resultList.get_embedded().getAdressen());

        List<AdressResponse> lstAdresses = (List<AdressResponse>) resultList.get_embedded().getAdressen();
        assertEquals(2, lstAdresses.size());

    }

    @Test
    public void testFindPersonenOpAdres2(){
        ResponseInfo responseInfo = handleGet(BASE_URL +"/adres/2");

        AdressResponse adressResponse = getJsonObject(responseInfo.getBody(), AdressResponse.class);
        assertNotNull(adressResponse);
        assertNotNull(adressResponse.get_links().get("personen").getHref());

        String uriPersonen = adressResponse.get_links().get("personen").getHref();
        responseInfo = handleGet(uriPersonen);


        log.info(responseInfo.getBody());

        ListPersonenResponse resultList = getJsonObject(responseInfo.getBody(), ListPersonenResponse.class);
        assertNotNull(resultList);

        List<PersonResponse>  personen = resultList.get_embedded().getPersonen();
        assertEquals(2, personen.size());

        assertEquals("Trompet",personen.get(0).getAchternaam());
        assertEquals("Donald",personen.get(0).getVoornaam());

        assertEquals("Trompet",personen.get(1).getAchternaam());
        assertEquals("Katrien",personen.get(1).getVoornaam());
    }

    @Test
    public void test003_InvalidService(){
        String targetUrl = BASE_URL+ "/unknown/1";

        ResponseEntity<String> result0 = restTemplate.getForEntity(targetUrl, String.class);
        assertEquals(404, result0.getStatusCodeValue());
        String data = result0.getBody();
        System.out.println(data);
        ErrorResponse errorResponse = getJsonObject(data, ErrorResponse.class);

        assertNotNull(errorResponse);
        assertEquals(404, errorResponse.getStatus().intValue());
        assertEquals("No message available", errorResponse.getMessage());

    }


}
