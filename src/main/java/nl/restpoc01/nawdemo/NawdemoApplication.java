package nl.restpoc01.nawdemo;

import nl.restpoc01.nawdemo.data.config.DataBaseConfiguration;
import nl.restpoc01.nawdemo.data.config.SpringSecurityConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan({"nl.restpoc01.nawdemo.controllers","nl.restpoc01.nawdemo.data.repository"})
@Import({ DataBaseConfiguration.class, SpringSecurityConfig.class})
@SpringBootApplication
@EnableJpaRepositories(basePackages = {
        "nl.restpoc01.nawdemo.data.repository"
})
public class NawdemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(NawdemoApplication.class, args);
    }

}
