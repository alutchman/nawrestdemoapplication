package nl.restpoc01.nawdemo.data.tables;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Data
@Entity
@Table(
        name="adress_tbl",
        uniqueConstraints=
        @UniqueConstraint(columnNames={"zipcode", "number", "extension"})
)
public class AdressEntity  {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    protected Long id;

    @Column( name = "streetname",nullable = false, length = 60)
    private String streetname;

    @Column( name = "number",nullable = false)
    private Integer number;

    @Column( name = "extension",nullable = false, length=10)
    private String extension;

    @Column( name = "city",nullable = false, length = 25)
    private String city;

    @Column( name = "zipcode",nullable = false, length = 8)
    private String zipcode;

    @Column( name = "phone", length = 20)
    private String phone;

    @OneToMany(mappedBy = "adres", cascade = CascadeType.ALL)
    private List<PersonEntity> personen = new ArrayList<>();


}
