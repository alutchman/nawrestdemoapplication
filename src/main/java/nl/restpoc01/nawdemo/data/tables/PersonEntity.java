package nl.restpoc01.nawdemo.data.tables;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.sql.Date;


@Data
@Entity
@Table(
		name="person_tbl",
		uniqueConstraints=@UniqueConstraint(columnNames={"achternaam", "voornaam", "geboortedatum", "adres_id"})
)
@EqualsAndHashCode(exclude={"year"})
public class PersonEntity {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	protected Long id;

	private String achternaam;

	private String voornaam;

	private Date geboortedatum;

	@ManyToOne
	@JoinColumn(name = "adres_id")
	private AdressEntity adres;

}
