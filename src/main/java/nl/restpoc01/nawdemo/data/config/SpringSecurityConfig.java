package nl.restpoc01.nawdemo.data.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.sql.DataSource;

@Import({DataBaseConfiguration.class})
@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private DataSource dataSource;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    // Create 2 users for demo
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource);

        /*
          //Next part is redundant when using default table names as shown below

          .usersByUsernameQuery("select username, password, enabled"
                        + " from users where username=?")
                .authoritiesByUsernameQuery("select username, authority "
                        + "from authorities where username=?")
                .passwordEncoder(passwordEncoder);
       */
        /*

            String encodedPw = passwordEncoder.encode("password");
            auth.inMemoryAuthentication()
                .withUser("user").password(encodedPw).roles("OPERATOR")
                .and()
                .withUser("admin").password(encodedPw).roles("OPERATOR", "ADMIN");

        */

    }

    // Secure the endpoins with HTTP Basic authentication
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                //HTTP Basic authentication
                .httpBasic()
                .and()
                .authorizeRequests()
                .antMatchers("/*").hasAnyRole("OPERATOR", "ADMIN")
                .antMatchers(HttpMethod.POST, "/adres").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/persoon").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, "/adres").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, "/persoon").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH, "/adres/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH, "/persoon/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/adres/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/persoon/**").hasRole("ADMIN")
                .and()
                .csrf().disable()
                .formLogin().disable();
    }

}
