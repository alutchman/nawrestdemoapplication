package nl.restpoc01.nawdemo.data.repository;



import nl.restpoc01.nawdemo.data.tables.AdressEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;


@RepositoryRestResource(collectionResourceRel = "adressen", path = "adres")
public interface AdressRepository extends PagingAndSortingRepository<AdressEntity, Long> {
    @Query("SELECT t FROM AdressEntity t where zipcode=?1 ")
    List<AdressEntity> findByZipcode(@Param("zipcode") String value);

    @Query("SELECT t FROM AdressEntity t where zipcode=?1 AND  number=?2")
    List<AdressEntity> findByZipcodeNumber(@Param("zipcode") String zipcode, @Param("number") int number);


}
