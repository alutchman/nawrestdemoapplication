package nl.restpoc01.nawdemo.data.repository;


import nl.restpoc01.nawdemo.data.tables.PersonEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "personen", path = "persoon")
public interface PersonRepo extends PagingAndSortingRepository<PersonEntity, Long> {
    @Query("SELECT t FROM PersonEntity t where achternaam=?1 ")
    List<PersonEntity> zoekopachternaam(@Param("achternaam") String value);

    @Query("SELECT t FROM PersonEntity t where achternaam=?1 AND voornaam=?2")
    List<PersonEntity> zoekopnaam(@Param("achternaam") String value1,@Param("voornaam") String value2);
}
