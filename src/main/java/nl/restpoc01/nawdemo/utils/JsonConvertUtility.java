package nl.restpoc01.nawdemo.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Slf4j
public class JsonConvertUtility {
    private static final JsonConvertUtility JSON_TEST_UTILITY = new JsonConvertUtility();
    private final ObjectMapper objectMapper = new ObjectMapper();

    private JsonConvertUtility(){
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    private String findJsonAsString(Object jsonTransferData){
        try {
            return objectMapper.writeValueAsString(jsonTransferData);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    private <E> E findJsonObject(String jsonTransferData, Class<E> clazz){
        try {
            return objectMapper.readValue(jsonTransferData, clazz);
        } catch (IOException e) {
            log.error(e.getMessage());
            return null;
        }
    }

    public static String getJsonAsString(Object jsonTransferData){
        return JSON_TEST_UTILITY.findJsonAsString(jsonTransferData);

    }


    public static <E> E getJsonObject(String jsonTransferData, Class<E> clazz){
        return JSON_TEST_UTILITY.findJsonObject(jsonTransferData, clazz);
    }




}
